<?php

require_once "DatabaseObject.php";

class Article implements DatabaseObject
{
    private $id = 0;
    private $title = '';
    private $inhalt = '';
    private $freigabedatum = '';
    private $besitzer_id = 0;

    private $errors = [];

    public function __construct()
    {
    }

    public function validate()
    {
        return $this->validateHelper('Titel', 'titel', $this->title, 255) &
            $this->validateHelper('Inhalt', 'inhalt', $this->inhalt, 255) &
            $this->validateHelper('Freigabedatum', 'freigabedatum', $this->freigabedatum, 255) &
            $this->validateHelperInt('Besitzer_ID', 'besitzer_id', $this->besitzer_id);
    }


    public function validateHelper($label, $key, $value, $maxLength)
    {
        if (strlen($value) == 0) {
            $this->errors[$key] = "$label darf nicht leer sein!";
            return false;
        } else if (strlen($value) > $maxLength) {
            $this->errors[$key] = "$label zu lang (maximal $maxLength Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    public function validateHelperInt($label, $key, $value)
    {
        if (!is_numeric($value)) {
            $this->errors[$key] = "$label muss eine Zahl sein!";
            return false;
        } else {
            return true;
        }
    }

    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }
            return true;
        }
        return false;
    }

    public function create()
    {
        $sql = "INSERT INTO article(title,inhalt,freigabedatum,besitzer_id) VALUES(?,?,?,?)";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($this->title, $this->inhalt, $this->freigabedatum, $this->besitzer_id));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $sql = "UPDATE article SET title = ?, inhalt = ?, freigabedatum = ?, besitzer_id = ? WHERE id=?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($this->title, $this->inhalt, $this->freigabedatum, $this->besitzer_id, $this->id));
        Database::disconnect();
    }

    public static function getUserArticle($id)
    {
        $sql = "SELECT name FROM user JOIN article ON user.id =?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($id));
        $items = $stat->fetch();
        $besitzer =array_unique($items,SORT_REGULAR);
        $besitzer = implode($besitzer);
        Database::disconnect();
        return $besitzer;
    }

    public static function get($id)
    {
        $sql = "SELECT * FROM article WHERE id=?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($id));
        $item = $stat->fetchObject('Article');
        Database::disconnect();
        return $item != false ? $item : null;
    }

    public static function getAll()
    {
        $sql = "SELECT * FROM article ORDER BY freigabedatum DESC";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute();
        $items = $stat->fetchAll(PDO::FETCH_CLASS, 'Article');
        Database::disconnect();
        return $items;
    }

    public static function delete($id)
    {
        $sql = "DELETE FROM article WHERE id=?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($id));
        Database::disconnect();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getInhalt()
    {
        return $this->inhalt;
    }

    /**
     * @param string $inhalt
     */
    public function setInhalt($inhalt)
    {
        $this->inhalt = $inhalt;
    }

    /**
     * @return string
     */
    public function getFreigabedatum()
    {
        return $this->freigabedatum;
    }

    /**
     * @param string $freigabedatum
     */
    public function setFreigabedatum($freigabedatum)
    {
        $this->freigabedatum = $freigabedatum;
    }

    /**
     * @return int
     */
    public function getBesitzerId()
    {
        return $this->besitzer_id;
    }

    /**
     * @param int $besitzer_id
     */
    public function setBesitzerId($besitzer_id)
    {
        $this->besitzer_id = $besitzer_id;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}