<?php

class Database
{
    private static $dbName = 'php32';
    private static $dbHost = 'mysql';
    private static $dbUsername = 'root';
    private static $dbUserPassword = '123';

    private static $conn = null;

    public function __construct()
    {
        exit('Init function is not allowed');
    }

    public static function connect()
    {
        // One connection through whole application
        if (null == self::$conn) {
            try {
                self::$conn = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
                //TODO: create database if not exist php32; Quelle: https://stackoverflow.com/questions/2583707/can-i-create-a-database-using-pdo-in-php

            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

    public static function disconnect()
    {
        self::$conn = null;
    }
}

