<?php

require_once "DatabaseObject.php";

class User implements DatabaseObject
{
    private $id = 0;
    private $name = '';
    private $email = '';
    private $password = '';

    private $errors = [];

    public function __construct()
    {
    }

    public function validate()
    {
        return $this->validateHelper('Name','name' , $this->name, 255) &
            $this->validateHelper('Email','email', $this->email, 255) &
            $this->validateHelper('Passwort','password' , $this->password, 255);
    }

    public function validateHelper($label, $key, $value, $maxLength)
    {
        if(strlen($value)==0){
            $this->errors[$key] = "$label darf nicht leer sein!";
            return false;
        } else if(strlen($value) > $maxLength){
            $this->errors[$key] = "$label zu lang (maximal $maxLength Zeichen)";
            return false;
        } else {
            return true;
        }
    }

    public function save()
    {
        if($this->validate()){
            if($this->id != null && $this->id >0){
                $this->update();
            } else {
                $this->id =$this->create();
            }
            return true;
        }
        return false;
    }

    public function create()
    {

        $sql = "INSERT INTO user(name,email,password) VALUES(?,?,?)";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($this->name, $this->email, $this->password));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    public function update()
    {
        $sql = "UPDATE user SET name = ?, email = ?, password = ? WHERE id=?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($this->name, $this->email, $this->password, $this->id));
        Database::disconnect();
    }

    public static function get($id)
    {
        $sql = "SELECT * FROM user WHERE id=?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($id));
        $item = $stat->fetchObject('User');
        Database::disconnect();
        return $item != false ? $item : null;
    }

    public static function getAll()
    {
        $u=new User();
        $u->setName("se");
        $sql = "SELECT * FROM user ORDER BY name ASC";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute();
        $items = $stat->fetchAll(PDO::FETCH_CLASS, 'User');
        Database::disconnect();
        return $items;
    }
    public static function getAllForDropdowm()
    {
        $sql = "SELECT * FROM user ORDER BY name ASC";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute();
        $items = $stat->fetchAll();
        Database::disconnect();
        return $items;
    }
    public static function delete($id)
    {
        $sql = "DELETE FROM user WHERE id=?";
        $db = Database::connect();
        $stat = $db->prepare($sql);
        $stat->execute(array($id));
        Database::disconnect();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}