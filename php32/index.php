<?php
session_start();
require_once "models/Database.php";
require_once "views/helper/Func.php";

$msg = "";
$db = Database::connect();
// https://www.mitrajit.com/php-login-pdo-connection/
if (isset($_POST['submitBtn'])) {
    $username = $_POST['name'];
    $password = $_POST['password'];
    //console_log($username);
    if ($username != '' && $password != '') {
        try {
            $query = "select * from user where name=? and password=?";
            $stmt = $db->prepare($query);
            //  $stmt->bindParam('name', $username, PDO::PARAM_STR);
            // $stmt->bindValue('password', $password, PDO::PARAM_STR);
            $stmt->execute(array($username, $password));

            //console_log($stmt);
            $count = $stmt->rowCount();
            $row = $stmt->fetch();
            if ($count == 1 && !empty($row)) {
                /******************** Your code ***********************/
                $_SESSION['sess_user_id'] = $row['id'];
                $_SESSION['sess_name'] = $row['name'];
                $_SESSION['isLoggedIn'] = true;
                unset($_SESSION['sess_invalid']);
                //console_log($row['name']);
                // console_log($_SESSION);
                // console_log(isset($_SESSION['sess_user_id']) ? $_SESSION['sess_user_id'] : '');
            } else {
                $msg = "Invalid username and password!";
                $_SESSION['sess_invalid'] = true;

                //   console_log($_SESSION['sess_invalid'] == true);
                if (!empty($msg)):
                    //   if (false):
                    ?>
                    <?php
                    echo '<script ="javascript">';
                    echo 'alert("Invalid username and password!")';
                    echo '</script>'
                    ?>
                    <div class="help-block"><?= $msg ?></div>
                <?php endif;
            }
        } catch (PDOException $e) {
            echo "Error : " . $e->getMessage();
        }
    } else {
        $msg = "Both fields are required!";
        $_SESSION['sess_invalid'] = true;
    }
} else {
    console_log("d");
    $_SESSION['sess_invalid'] = true;

}

if (isset($_POST['logout'])) {
    header("Location: logout.php");
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Awesome CMS</title>

    <link rel="shortcut icon" href="css/css-old/favicon.ico" type="image/x-icon">
    <link rel="icon" href="css/css-old/favicon.ico" type="image/x-icon">

    <link href="css/css-old/bootstrap.min.css" rel="stylesheet">
    <link href="css/css-old/index.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="views/article/index.php">Beiträge</a></li>
                <li><a href="#">Benutzer</a></li>
            </ul>
            <?php
            if (!isset($_SESSION['isLoggedIn'])) {
                ?>

                <form class="navbar-form navbar-right" action="index.php" method="post">
                    <div class="row">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            </div>
                            <div class="col mx-2">
                                <input type="text" class="form-control " placeholder="name" name="name">
                                <input type="password" class="form-control mx-5" placeholder="password" name="password">
                                <button class="btn btn-primary" type="submit" name="submitBtn">Anmelden</button>
                            </div>
                        </div>
                    </div>
                </form>

                <?php
            } elseif (isset($_SESSION['sess_invalid']) && $_SESSION['sess_invalid'] == true && $_SESSION['isLoggedIn'] != true) {
                console_log("d");
                ?>

                <form action="index.php" class="navbar-form navbar-right input-group" method="post">
                    <input type="text" placeholder="name" name="name">
                    <input type="password" placeholder="password" name="password">
                    <button class="btn btn-primary" type="submit" name="submitBtn">Anmelden</button>
                </form>

                <div class="help-block alert">Invalid username and password!</div>
                <?php
            } elseif ($_SESSION['isLoggedIn']) {

                ?>
                <form class="navbar-form navbar-right" action="index.php" method="post">
                    <button class="btn btn-warning" type="submit" name="logout">Abmelden</button>
                </form>
                <?php
            }
            ?>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>

<div class="jumbotron">
    <div class="container">
        <h1>Hello Awesome CMS!</h1>
        <p>This is a template for a simple marketing or informational website. It includes a large callout called a
            jumbotron and three supporting pieces of content. Use it as a starting point to create something more
            unique.</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-4">
            <h2>Article 1</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Article 2</h2>
            <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris
                condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis
                euismod. Donec sed odio dui. </p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Article 3</h2>
            <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula
                porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
                fermentum massa justo sit amet risus.</p>
            <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
        </div>
    </div>

    <hr>
    <?php include "views/helper/footer.php"; ?>
</div> <!-- /container -->

</body>
</html>