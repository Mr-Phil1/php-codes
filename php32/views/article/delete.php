<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
require_once "../helper/Func.php";
session_start();
?>

<body>

<?php

require_once "../../models/Article.php";
if (!isset($_SESSION['isLoggedIn'])) {
    header("Location: ../../index.php");
    console_log("User not logged in", true);
}
$id = !empty($_GET['id']) ? $_GET['id'] : 0;

if(!empty($_POST['id'])){

    Article::delete($_POST['id']);

    header("Location: index.php");
    exit();
} else {
    $a = Article::get($id);
}

?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Beiträge</a></li>
                <li><a href="#">Benutzer</a></li>
            </ul>
            <form class="navbar-form navbar-right" action="index.php" method="post">
                <button class="btn btn-warning" type="submit" name="logout">Abmelden</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>
<div class="container">
    <h2>Beitrag löschen</h2>

    <form class="form-horizontal" action="delete.php?id=<?= $a->getId() ?>" method="post">
        <input type="hidden" name="id" value="<?= $a->getId() ?>"/>
        <p class="alert alert-error">Wollen Sie den Beitrag <?= $a->getTitle() ?>  wirklich löschen?</p>
        <div class="form-actions">
            <button type="submit" class="btn btn-danger">Löschen</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>
    <div>
        <br>
        <?php include "../helper/footer.php"; ?>
    </div>
</div> <!-- /container -->

</body>
</html>