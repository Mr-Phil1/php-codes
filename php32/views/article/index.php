<!DOCTYPE html>
<html lang="de">
<?php
//http://localhost/php-codes/php32/views/article/index.php

require_once "../helper/Func.php";
session_start();
include "../helper/head.php";
if (!isset($_SESSION['isLoggedIn'])) {
    header("Location: ../../index.php");
    console_log("User not logged in", true);
}

if (isset($_POST['logout'])) {
    header("Location: ../../logout.php");
}
/*
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    exit();
} else {
    $c = Credentials::get($_GET['id']);
}
//Überprüfung ob das Objekt richtig geladen wurde!
if ($c == null) {
    http_response_code(404);
    die();
}*/
//print_r($c);

?>

<body>

<?php

require_once "../../models/Article.php";
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Beiträge</a></li>
                <li><a href="#">Benutzer</a></li>
            </ul>
            <form class="navbar-form navbar-right" action="index.php" method="post">
                <button class="btn btn-warning" type="submit" name="logout">Abmelden</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>
<br><br>
<div class="container">
    <div class="row">
        <h2>Beiträge</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Titel</th>
                <th>Inhalt</th>
                <th>Besitzer</th>
                <th>Freigabedatum</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $article = Article::getAll();
            // var_dump($article);
            foreach ($article as $a) {
                echo '<tr>';
                echo '<td>' . $a->getTitle() . '</td>';
                echo '<td>' . $a->getInhalt() . '</td>';
                echo '<td>' . $a->getUserArticle($a->getBesitzerId()) . '</td>';
                echo '<td>' . $a->getFreigabedatum() . '</td>';
                echo '<td>';
                echo '<a class="btn btn-info" href="view.php?id=' . $a->getId() . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                echo '&nbsp;';
                echo '<a class="btn btn-primary" href="update.php?id=' . $a->getId() . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                echo '&nbsp;';
                echo '<a class="btn btn-danger" href="delete.php?id=' . $a->getId() . '"><span class="glyphicon glyphicon-remove"></span></a>';
                echo '&nbsp;';
                echo '</td>';
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php include "../helper/footer.php"; ?>
</div> <!-- /container -->

</body>
</html>