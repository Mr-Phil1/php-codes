<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
require_once "../helper/Func.php";
session_start();
?>

<body>

<?php
include "../helper/navbar.php";

require_once "../../models/Article.php";
require_once "../../models/User.php";
if (!isset($_SESSION['isLoggedIn'])) {
    header("Location: ../../index.php");
    console_log("User not logged in", true);
}
if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} else if (!is_numeric($_GET['id'])) {
    http_response_code(400);
    die();
} else {
    $a = Article::get($_GET['id']);
}
if ($a == null) {
    http_response_code(404);
    die();
}

if (!empty($_POST)) {

    $a->setTitle(isset($_POST['title']) ? $_POST['title'] : '');
    $a->setFreigabedatum(isset($_POST['releasedate']) ? $_POST['releasedate'] : '');
    $a->setBesitzerId(isset($_POST['owner']) ? $_POST['owner'] : '');
    $a->setInhalt(isset($_POST['content']) ? $_POST['content'] : '');
    if ($a->save()) {
        header("Location: view.php?id=" . $a->getId());
        exit();
    }
}

if (isset($_POST['logout'])) {
    header("Location: ../../logout.php");
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Beiträge</a></li>
                <li><a href="#">Benutzer</a></li>
            </ul>
            <form class="navbar-form navbar-right" action="index.php" method="post">
                <button class="btn btn-warning" type="submit" name="logout">Abmelden</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>
<div class="container">
    <div class="row">
        <h2>Beitrag bearbeiten</h2>
    </div>

    <form class="form-horizontal" action="update.php?id=<?= $a->getId() ?>" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($a->getErrors()['titel']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45" value="<?= $a->getTitle() ?>">
                    <?php if (!empty($a->getErrors()['titel'])): ?>
                        <div class="help-block"><?= $a->getErrors()['titel'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required <?php echo !empty($a->getErrors()['freigabedatum']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate" value="<?= $a->getFreigabedatum() ?>">
                    <?php if (!empty($a->getErrors()['freigabedatum'])): ?>
                        <div class="help-block"><?= $a->getErrors()['freigabedatum'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required <?php echo !empty($a->getErrors()['besitzer_id']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Besitzer *</label>
                    <input type="number" readonly class="form-control" name="owner" value="<?= $_SESSION['sess_user_id'] ?>">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required <?php echo !empty($a->getErrors()['inhalt']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10"><?= $a->getInhalt() ?></textarea>
                    <?php if (!empty($a->getErrors()['inhalt'])): ?>
                        <div class="help-block"><?= $a->getErrors()['inhalt'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Aktualisieren</button>
            <a class="btn btn-default" href="index.php">Abbruch</a>
        </div>
    </form>
    <?php include "../helper/footer.php";?>
</div> <!-- /container -->

</body>
</html>