<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
require_once "../helper/Func.php";
session_start();
if (!isset($_SESSION['isLoggedIn'])) {
    header("Location: ../../index.php");
    console_log("User not logged in", true);
}
?>

<body>

<?php


require_once "../../models/Article.php";

$a = new Article();

//http://localhost/php-codes/php32/views/article/create.php
if (!empty($_POST)) {

    $a->setTitle($_POST['title'] ?? '');
    $a->setFreigabedatum($_POST['releasedate'] ?? '');
    $a->setBesitzerId($_POST['owner'] ?? '');
    $a->setInhalt($_POST['content'] ?? '');

    if ($a->save()) {
        header("Location: view.php?id=" . $a->getId());
        exit();
    }
    //  var_dump($a);
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Beiträge</a></li>
                <li><a href="#">Benutzer</a></li>
            </ul>
            <form class="navbar-form navbar-right" action="index.php" method="post">
                <button class="btn btn-warning" type="submit" name="logout">Abmelden</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>

<div class="container">
    <div class="row">
        <h2>Beitrag erstellen</h2>
    </div>

    <form class="form-horizontal" action="create.php" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($a->getErrors()['titel']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Titel *</label>
                    <input type="text" class="form-control" name="title" maxlength="45" value="<?= $a->getTitle() ?>">
                    <?php if (!empty($a->getErrors()['titel'])): ?>
                        <div class="help-block"><?= $a->getErrors()['titel'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-2">
                <div class="form-group required <?php echo !empty($a->getErrors()['freigabedatum']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Freigabedatum *</label>
                    <input type="date" class="form-control" name="releasedate" value="<?= $a->getFreigabedatum() ?>">
                    <?php if (!empty($a->getErrors()['freigabedatum'])): ?>
                        <div class="help-block"><?= $a->getErrors()['freigabedatum'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-3">
                <div class="form-group required <?php echo !empty($a->getErrors()['besitzer_id']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Besitzer *</label>
                    <input type="number" readonly class="form-control" name="owner"
                           value="<?= $_SESSION['sess_user_id'] ?>">
                    <?php if (!empty($a->getErrors()['besitzer_id'])): ?>
                        <div class="help-block"><?= $a->getErrors()['besitzer_id'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group required <?php echo !empty($a->getErrors()['inhalt']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Inhalt *</label>
                    <textarea class="form-control" name="content" rows="10" value="<?= $a->getInhalt() ?>"></textarea>
                    <?php if (!empty($a->getErrors()['inhalt'])): ?>
                    <div class="help-block"><?= $a->getErrors()['inhalt'] ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success">Erstellen</button>
                <a class="btn btn-default" href="index.php">Abbruch</a>
            </div>
    </form>
  <?php include "../helper/footer.php";?>
</div> <!-- /container -->

</body>
</html>