<!DOCTYPE html>
<html lang="de">
<?php
require_once "../helper/Func.php";
session_start();
include "../helper/head.php";
//http://localhost/php-codes/php32/views/article/view.php
?>

<body>

<?php
require_once "../../models/Article.php";

if (empty($_GET['id'])) {
    header("Location: index.php");
    exit();
} elseif (!is_numeric($_GET['id'])){
    http_response_code(400);
    die();
} else {
    $a = Article::get($_GET['id']);
}
if ($a == null) {
    http_response_code(404);
    die();
}
//var_dump($a);
include "../helper/navbar.php";
if (!isset($_SESSION['isLoggedIn'])) {
    header("Location: ../../index.php");
    console_log("User not logged in", true);
}

if (isset($_POST['logout'])) {
    header("Location: ../../logout.php");
}
?>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Awesome CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Beiträge</a></li>
                <li><a href="#">Benutzer</a></li>
            </ul>
            <form class="navbar-form navbar-right" action="index.php" method="post">
                <button class="btn btn-warning" type="submit" name="logout">Abmelden</button>
            </form>
        </div><!--/.navbar-collapse -->
    </div>
</nav>
<br><br>
<div class="container">
    <h2>Beitrag anzeigen</h2>

    <p>
        <a class="btn btn-primary" href="update.php?id=<?= $a->getId() ?>">Aktualisieren</a>
        <a class="btn btn-danger" href="delete.php?id=<?= $a->getId() ?>">Löschen</a>
        <a class="btn btn-default" href="index.php">Zurück</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Titel</th>
            <td><?= $a->getTitle() ?></td>
        </tr>
        <tr>
            <th>Freigabedatum</th>
            <td><?= $a->getFreigabedatum() ?></td>
        </tr>
        <tr>
            <th>Besitzer</th>
            <td><?= $a->getBesitzerId() ?></td>
        </tr>
        <tr>
            <th>Inhalt</th>
            <td><?= $a->getInhalt() ?></td>
        </tr>
        </tbody>
    </table>
    <?php include "../helper/footer.php";?>
</div> <!-- /container -->

</body>
</html>