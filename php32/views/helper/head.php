<head>
    <meta charset="utf-8">
    <title>Awesome CMS</title>

    <link rel="shortcut icon" href="../../css/css-old/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../../css/css-old/favicon.ico" type="image/x-icon">

    <link href="../../css/css-old/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/css-old/index.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
</head>