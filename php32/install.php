<?php
//http://localhost/php-codes/php32/install.php
include "models/Database.php";
$db = Database::connect();

$stmt = "CREATE TABLE `user`  (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
";
$db->exec($stmt);

$stmt = "ALTER TABLE `user` ADD PRIMARY KEY (`id`);";
$db->exec($stmt);

$stmt = "ALTER TABLE `user`  MODIFY `id` int NOT NULL AUTO_INCREMENT;";
$db->exec($stmt);

$stmt = "INSERT INTO `user` ( `name`, `email`, `password`) VALUES
('Günther Mair', 'guenther@mair.com', 'TestiTest'),
('Susi Schnell', 'schnell.susi@test.lab', 'TestiTest'),
('Tom', 'tom@itkolleg.at', '123');";

$db->exec($stmt);

$stmt = "CREATE TABLE `article` (
  `id` int NOT NULL,
  `title` varchar(255) NOT NULL,
  `inhalt` varchar(255) NOT NULL,
  `freigabedatum` varchar(255) NOT NULL,
  `besitzer_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
";
$db->exec($stmt);

$stmt = "ALTER TABLE `article` ADD PRIMARY KEY (`id`);";
$db->exec($stmt);

$stmt = "ALTER TABLE `article`  MODIFY `id` int NOT NULL AUTO_INCREMENT;";
$db->exec($stmt);
