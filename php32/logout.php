<?php
session_start();
unset($_SESSION['sess_user_id']);
unset($_SESSION['isLoggedIn']);
unset($_SESSION['sess_user_name']);
unset($_SESSION['sess_name']);
unset($_SESSION['sess_invalid']);
session_destroy();

header("Location: index.php");

echo "Logout erfolgreich";

