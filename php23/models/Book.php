<?php
require_once "lib/json.php";


class Book
{


    private $id = '';
    private $title = '';
    private $price = '';
    private $stock = '';
    private $origStock = '';

    /**
     * @param string $id
     * @param string $title
     * @param string $price
     * @param string $stock
     */
    public function __construct($id, $title, $price, $stock, $origStock)
    {
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->stock = $stock;
        $this->origStock = $origStock;
    }


    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * $this->inputIntoBookArray();
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOrigStock()
    {
        return $this->origStock;
    }

    /**
     * @param string $origStock
     */
    public function setOrigStock($origStock)
    {
        $this->origStock = $origStock;
    }



    public function setInStock($new)
    {
        $this->stock = $new;
    }


    public static function getAll()
    {
        $bookData = getJsonDataAsArray('lib/bookdata.json', false);
        $allBooks = array();
        foreach ($bookData as $book) {
            array_push($allBooks, new Book($book->id, $book->title, $book->price, $book->stock, $book->stock));
        }
        return $allBooks;

    }

    public static function get($id)
    {
        $allBooks = self::getAll();
        foreach ($allBooks as $book) {
            if ($book->getId() == $id) {
                return $book;
            }
        }
        return null;
    }
}