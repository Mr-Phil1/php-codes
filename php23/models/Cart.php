<?php
include "models/CartItem.php";

class Cart
{
    public $list=[];

    public function __construct()
    {
        $this->loadCookie();
    }

    private function loadCookie()
    {
        if (isset($_COOKIE['all'])) {
            $this->list = unserialize($_COOKIE['all']);
        }
    }

    private function saveCookie()
    {
        setcookie('all', serialize($this->list), time() + 3600);
    }

    public function add($book, $count)
    {
         $this->loadCookie();
        foreach ($this->list as $item) {
            if ($item->getBook()->getId() == $book->getId()) {
                $item->getAmount($item->getAmount() + $count);
                $this->saveCookie();
                return;
            }
        }
        array_push($this->list, new CartItem($book, $count));
        $this->saveCookie();
    }

    public function remove($id)
    {
       $this->loadCookie();
        $i = 0;
        foreach ($this->list as $cartItem) {
            if ($cartItem->getBook()->getId() == $id) {
                unset($this->list[$i]);
            }
            $i++;
        }
        $this->list = array_values($this->list);
        $this->saveCookie();
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }


}