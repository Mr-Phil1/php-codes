<?php
session_start();
include "models/Book.php";
include "models/Cart.php";
//printJson();
//print_r(getJsonDataAsArray('lib/bookdata.json', false));
$cart = new Cart();
for ($i = 1; $i <= count(Book::getAll()); $i++) {
    $submitName = "submit$i";
    if (isset($_POST[$submitName])) {
        $book = Book::get($_POST['id' . $i]);
        if ($_POST['select' . $i] > 0) {
            $cart->add($book, $_POST['select' . $i]);
            header("Location: warenkorb.php");
        }
    }
    unset($_POST[$submitName]);
    unset($_POST['select' . $i]);
}
?>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Warenkorb</title>
</head>
<body>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-sm m-3 border border-2 rounded align-content-center bg-light bg-gradient bg-opacity-75 shadow-lg rounded-2 ">
            <h1 class="text-center align-content-center">Warenkorb</h1>
            <form action="index.php" method="post">
                <div class="form form-group">
                    <div>
                        <input type="button" class="btn btn-secondary col-sm-3" id="warenkorb" name="warenkorb"
                               value="Warenkorb: <?= sizeof($cart->getList()) ?>"
                               onclick="location.href='warenkorb.php'">
                    </div>
                    <table class="table table-striped  table-hover col-sm-12  col-md-6 text-center">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Original Stock</th>
                            <th>Price</th>
                            <th>Select Stock</th>
                            <th>Submit Book</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $books = Book::getAll();
                        $i = 1;
                        $cartList = $cart->getList();
                        //  var_dump($books);
                        foreach ($books as $book) {
                            foreach ($cartList as $list) {
                                if ($list->getBook()->getId() == $book->getId()) {
                                    $book->setStock($book->getStock() - $list->getAmount());
                                }
                            }
                            echo "<tr>";
                            echo "<input type='hidden' name='id$i' id='id$i' value=" . $book->getId() . ">";
                            echo "<td class='text-start'>" . $book->getTitle() . "</td>";
                            echo "<td>" . $book->getOrigStock() . "</td>";
                            echo "<td> €" . number_format($book->getPrice(), 2) . "</td>";
                            echo "<td><select name='select$i' id='select$i'>" . createDropDown($book->getStock()) . "</select></td>";
                            echo "<td><button type='submit' name='submit$i' class='btn btn-dark'>Submit Book</button></td>";
                            $i++;
                        }
                        function createDropDown($getStock)
                        {
                            $auswahl = '';
                            if ($getStock == 0) {
                                $auswahl .= "<option id='count' value='0'>Ausverkauft</option>";
                            } else {
                                for ($i = 0; $i <= $getStock; $i++) {
                                    $auswahl .= "<option id='count' value='$i'>" . $i . "</option>";
                                }
                            }
                            return $auswahl;

                        }

                        ?>
                        </tbody>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>
