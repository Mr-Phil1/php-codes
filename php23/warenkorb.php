<?php
session_start();
include "models/Cart.php";
include "models/Book.php";
$cart = new Cart();
$cartList = $cart->getList();
$overallPrice = 0.0;

for ($i = 1; $i <= count(Book::getAll()); $i++) {
    $submitName = "submit$i";
    if (isset($_POST[$submitName])) {
        $book = Book::get($_POST['id' . $i]);
        $cart->remove($book->getId());
        header("Location: warenkorb.php");
    }
    unset($_POST[$submitName]);
    unset($_POST['select' . $i]);
}
?>

<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <title>Warenkorb</title>
</head>
<body>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-sm m-3 border border-2 rounded align-content-center bg-light bg-gradient bg-opacity-75 shadow-lg rounded-2 ">
            <h1 class="text-center align-content-center">Warenkorb</h1>
            <form action="warenkorb.php" method="post">
                <div class="form form-group">
                    <div>
                        <input type="button" class="btn btn-secondary col-sm-3" id="back" name="back"
                               value="Zurück"
                               onclick="location.href='index.php'">
                    </div>

                    <table class="table table-striped  table-hover col-sm-12  col-md-6 text-center">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Amount</th>
                            <th>Price per Book</th>
                            <th>Full Price</th>
                            <th>Remove Book</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                       //   var_dump($cartList);
                        $i = 1;
                        foreach ($cartList as $item) {
                            // var_dump($item);
                            //var_dump($overallPrice);
                            $book = $item->getBook();
                            $fullPrice = $book->getPrice() * $item->getAmount();
                            //   var_dump($book);
                            echo "<tr>";
                            echo "<input type='hidden' name='id$i' id='id$i' value=" . $book->getId() . ">";
                            echo "<td class='text-start'>" . $book->getTitle() . "</td>";
                            echo "<td>" . $item->getAmount() . "</td>";
                            echo "<td> €" . number_format($book->getPrice(), 2) . "</td>";
                            echo "<td> €" . number_format($fullPrice, 2) . "</td>";
                            echo "<td><button type='submit' name='submit$i' class='btn btn-danger'>Remove Book</button></td>";
                            $i++;
                            $overallPrice += $fullPrice;
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="col-sm-6 col-md-4 m-3 border border-2 rounded align-content-center bg-light bg-gradient bg-opacity-75 shadow-lg rounded-2 text-center align-content-center">
                       Full Price of the Cart: € <?= number_format($overallPrice, 2)  ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>