<?php
/**
 * @param $jsonFIleInput
 * @param $arrayOn
 */
function printJson($jsonFIleInput, $arrayOn)
{
    // https://stackoverflow.com/a/5164459
    $jsonFile = file_get_contents($jsonFIleInput);
    $decode1 = json_decode($jsonFile, $arrayOn);
    echo "<pre>";
    print_r($decode1);
}


/**
 * @param $jsonFIleInput "e.g 'lib/bookdata.json'"
 * @param $arrayOn "e.g  true/false"
 * @return mixed
 */
function  getJsonDataAsArray($jsonFIleInput, $arrayOn){
    $jsonFile = file_get_contents($jsonFIleInput);
    return json_decode($jsonFile, $arrayOn);
}