<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- Einbindung JavaScript -->
    <script type="text/javascript" src="js/index.js"></script>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>

    <title>Benutzerdetails</title>

</head>
<body>
<!--Layout-->
<?php
include("lib/func.php");
$user = getDataPerId($_GET['id']);

?>

<div class="container">
    <h1 class="mt-5 mb-3">Benutzerdetails</h1>


    <div class="row">

        <div class="col-1">
            <a href="index.php" type="sumbit" class="text-decoration-none">zurück</a>
        </div>

    </div>

    <div class="row">


        <table class="table table-striped">
            <tbody>
            <tr>
                <th><?= " Vorname" ?></th>
                <th><?= $user['firstname'] ?></th>
            </tr>
            <tr>
                <th><?= "Nachname" ?></th>
                <th><?= $user['lastname'] ?></th>
            </tr>
            <tr>
                <th><?= "Geburtsdatum" ?></th>
                <th><?= date("d.m.Y",strtotime($user['birthdate'])) ?></th>
            </tr>
            <tr>
                <th><?= "E-Mail" ?></th>
                <th><?= $user['email'] ?></th>
            </tr>
            <tr>
                <th><?= "Telefon" ?></th>
                <th><?= $user['phone'] ?></th>

            </tr>
            <tr>
                <th><?= "Straße" ?></th>
                <th><?= $user['street'] ?></th>
            </tr>
            </tbody>
        </table>
        <div class="col-8"></div>
    </div>

</div>
</body>
</html>