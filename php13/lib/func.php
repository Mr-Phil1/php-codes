<?php
require("lib/userdata.php");
function getAllData()
{
    global $data;
    return $data;
}

function getDataPerId($id)
{
    global $data;
    $user = $data[$id - 1];
    return $user;
}

/**
 * Gibt die übergebene Message in Form eines Allerts aus.
 * @param $errorMessage übergebene Error Message.
 */
function printAlert($errorMessage)
{
    echo "<div class='alert alert-danger mt-2'>" . $errorMessage . "</div>";
}
function getInfo()
{
    phpinfo();
}

function getFilteredData($filter)
{
    global $data;
    $filterData = [];
    foreach ($data as $user => $inhalt) {
        $filterFN = stripos($inhalt['firstname'], $filter);
        $filterLN = stripos($inhalt['lastname'], $filter);
        $filterEmail = stripos($inhalt['email'], $filter);
        $filterBirthday = stripos($inhalt['birthdate'], $filter);
        if ($filterFN !== false | $filterLN !== false | $filterEmail !== false | $filterBirthday !== false) {
            $filterData[] = $inhalt;
        }
    }
    return $filterData;
}


