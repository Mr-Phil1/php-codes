<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/index.js"></script>
    <title>Benutzerdaten suchen</title>
</head>
<body>
<div class="container">

    <h1 class="mt-5 mb-3">Benutzerdaten suchen</h1>
    <form action="index.php" method="post">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <label for="search" class="col-sm-1 col-form-label">Suchen</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="search" name="search">
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-12">
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="Suchen">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 ml-3">
                            <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        include("lib/func.php");

        ?>
        <div class="row">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>E-Mail</th>
                    <th>Geburtsdatum</th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once("lib/func.php");
                getAllData();
                $search = '';
                $e = [];

                if (isset($_POST['submit'])) {
                    $search = $_POST['search'] ?? '';
                    $e = getFilteredData($search);
                    if (empty($e)){
                        printAlert("Keine Einträge gefunden");
                    }
                } else {
                    $e = getAllData();
                }

                foreach ($e as $d) {
                    echo "<tr>";
                    echo "<td><a href=\"details.php?id={$d['id']}\">{$d['firstname']} {$d['lastname']}</a></td>";
                    echo "<td>" . $d['email'] . "</td>";
                    echo "<td>" . date("d.m.Y", strtotime($d['birthdate'])) . "</td>";
                    echo "</tr>";
                }
                ?>
                </tbody>
            </table>
        </div>
    </form>
</div>
</div>
</body>
</html>