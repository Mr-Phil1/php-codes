function validateMeasurementDate(elem) {
    let today = new Date();
    today.setHours(0, 0, 0, 0);
    let messDatum = new Date(elem.value);
    messDatum.setHours(0, 0, 0, 0);
    if (messDatum <= today) {
        elem.classList.add("is-valid");
        elem.classList.remove("is-invalid");
    } else {
        elem.classList.add("is-invalid");
        elem.classList.remove("is-valid");
    }
}