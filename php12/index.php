<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="js/index.js"></script>
    <title>BMI-Rchner</title>
</head>
<body>
<div class="container">
    <h1 class="mt-5 mb-3">Body-Mass-Index-Rechner</h1>
    <?php
    require "lib/func.php";

    $name = '';
    $measurementDate = '';
    $height = '';
    $weight = '';
    if (isset($_POST['submit'])) {
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $measurementDate = isset($_POST['measurementDate']) ? $_POST['measurementDate'] : '';
        $height = isset($_POST['height']) ? $_POST['height'] : '';
        $weight = isset($_POST['weight']) ? $_POST['weight'] : '';
        if (validate($name, $measurementDate, $height, $weight)) {
            #echo "<p class='alert alert-success'>Alle eingegebenen Daten sind korrekt!</p>";
            printBmi($height, $weight);
        } else {
            echo "<div class='alert alert-danger'><p> Die eingegebenen Daten sind in fehlerhaft!</p><ul>";
            foreach ($errors as $key => $value) {
                echo "<li>" . $value . "</li>";
            }
            echo "</ul></div>";
        }
    }
    ?>
    <form id="form_bmi" action="index.php" method="post">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-sm-8 form-group">
                        <label for="name">Name*</label>
                        <input id="name" name="name" type="text"
                               class="form-control <?= isset($errors['name']) ? 'is-invalid' : '' ?>" maxlength="25"
                               value="<?= htmlspecialchars($name) ?>" required>
                    </div>
                    <div class="col-sm-4 form-group">
                        <label for="measurementDate">Messdatum*</label>
                        <input id="measurementDate" name="measurementDate" type="date"
                               class="form-control <?= isset($errors['messdatum']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($measurementDate) ?>"
                               onchange="validateMeasurementDate(this)" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <label for="height">Größe (cm)*</label>
                        <input id="height" name="height" type="number"
                               class="form-control <?= isset($errors['height']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($height) ?>" min="1" max="300" required>
                    </div>
                    <div class="col-sm-6 form-group">
                        <label for="weight">Gewicht (kg)*</label>
                        <input id="weight" name="weight" type="number"
                               class="form-control <?= isset($errors['weight']) ? 'is-invalid' : '' ?>"
                               value="<?= htmlspecialchars($weight) ?>" min="1" max="500" required>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Info zum BMI:</h4>
                        <p>Unter 18.5: Untergewicht</p>
                        <p>18.5 - 24.9: Normal</p>
                        <p>25.0 - 29.9: Übergewicht</p>
                        <p>30.0 und darüber Adipositas</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-sm-3 mb-3">
                <input type="submit" name="submit" class="btn btn-primary btn-block" value="Speichern">
            </div>
            <div class="col-sm-3">
                <a href="index.php" class="btn btn-secondary btn-block">Löschen</a>
            </div>
        </div>
    </form>
</div>
</div>
</body>
</html>
