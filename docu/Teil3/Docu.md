---
title: PHP Grundlagen
subtitle: Teil 3 - Datenbankzugriff / ORM - Docu
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS; PHP; Datenbankzugriff
titlepage: true
titlepage-color: "E19494"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

# SQL

Ist die Standart Sprache für den Zugriff und die Bearbeitung von Datenbanken

## Was ist SQL

- Ist sein 1986 ein **ANSI** (American National Standards Institute) und seit 1987 ein **ISO** (International Organization for Standardization) Standard.
- Steht für **S**tructured **Q**uery **L**anguage

## Was kann SQL

- SQL can execute queries against a database
- SQL can retrieve data from a database
- SQL can insert records in a database
- SQL can update records in a database
- SQL can delete records from a database
- SQL can create new databases
- SQL can create new tables in a database
- SQL can create stored procedures in a database
- SQL can create views in a database
- SQL can set permissions on tables, procedures, and views

> Quelle: [Introduction to SQL](https://www.w3schools.com/sql/sql_intro.asp) -> What Can SQL do?-

## Einfache Abfrage

```sql
SELECT * FROM Student;
```

## Einfügen von Datensätzen

```sql
INSERT INTO Student (Vorname, Nachname, MatrNr) VALUES ('Günther', 'Maier', 15);
```

\pagebreak

# PHP

Für die Verbindung zu einer MySQL-Datenbank gib es in PHP (ab Version 5) zwei Möglichkeiten:

- MySQLi extension
  - Unterstützt nur MySQL
- PDO (PHP Data Objects)
  - Unterstützt 12 verschiedene Datenbank Systeme

## MySQLi Verbindung aufbauen

```php
<?php
$servername = "localhost";
$username = "username";
$password = "password";

// Create connection
$conn = mysqli_connect($servername, $username, $password);

// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}
echo "Connected successfully";
?>
```

\pagebreak

## PDO

### Verbindung aufbauen

```php
$dbName = 'php12';
$dbHost = 'mysql';
$dbUsername = 'root';
$dbUserPassword = '123';

/**
 * Verbindung zur DB aufbauen
 * @return PDO (Verbindungsobjekt)
 */
function connect()
{
    global $dbName, $dbHost, $dbUsername, $dbUserPassword, $dsn, $dbPort, $pod;
    try {
        $conn = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUsername, $dbUserPassword);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "<p class='alert alert-success'>Datenbank Connection erfolgreich |</p>";
        return $conn;
    } catch (PDOException $e) {
        echo "<p class='alert alert-danger'>Datenbank Connection Problem </p>";
        die($e->getMessage());
    }
}
```

### Prepared Statements

Sind Vorbereitete SQL-Befehle mit anonymen Parametern, damit kann man SQL-Injktions besser umgehen und die Datenbank diesbezüglich absichern. Mittels des **execute** Befehls werden dann die gewünschen Parameter mitgegeben (siehe: ).

```php
  public function create()
    {
        $sql = "INSERT INTO credentials (name, domain, cms_username, cms_password) VALUES (?,?,?,?)";
        $db = Database::connect();
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->name, $this->domain, $this->cms_username, $this->cms_password));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }
```

\pagebreak

# ORM

Steht für Object-Relational Mapping und ist eine Programmiertechnik für das Konvertieren von Daten zwischen Relationalen Datenbanken und objektorientierten Programmiersprachen. Bei diesem Prinzip wird für jede Klasse eine Datenbank-Tabelle erstellt.

\pagebreak

# Literatur-/ Quellen-/ Abbildungsverzeichnis

## Literaturverzeichnis

- [W3Schools](https://www.w3schools.com/)
  - [SQL Tutorial](https://www.w3schools.com/sql/default.asp)
  - [PHP MySQL Database](https://www.w3schools.com/php/php_mysql_intro.asp)
- [TutorialsDesk](https://www.tutorialsdesk.com/)
  - [Object-Relational Mapping (ORM) Tutorial](https://www.tutorialsdesk.com/2014/10/object-relational-mapping-orm-tutorial.html)
- PHP
  - [PHP-Einfach.de - MySQL Tutorial](https://www.php-einfach.de/mysql-tutorial/)
  - [PHP-Einfach.de - Crashkurs PDO](https://www.php-einfach.de/mysql-tutorial/crashkurs-pdo/)
  - [Laracasts - The PHP Practitioner](https://laracasts.com/series/php-for-beginners)
- [Wikipedia](https://de.wikipedia.org/)
  - [Objektrelationale Abbildung](https://de.wikipedia.org/wiki/Objektrelationale_Abbildung)
  - [Active record pattern](https://en.wikipedia.org/wiki/Active_record_pattern)

## Abbildungsvezeichnis

\renewcommand{\listfigurename}{}
\listoffigures
