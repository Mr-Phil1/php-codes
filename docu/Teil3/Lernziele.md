---
title: PHP Grundlagen
subtitle: Teil 3 - Datenbankzugriff / ORM - Lernziele + Expertenstatements
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS; PHP; Datenbankzugriff
titlepage: true
titlepage-color: "E19494"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

# Du wirst folgende Hauptkompetenz erreichen (Hauptziel)

- Du kannst für neue, authentische Problemstellungen datenbankgestützte Webapplikationen mit PHP implementieren.

# Du wirst folgende Erkenntnisse erzielen (Erkenntnisziele)

- Du kannst PHP zusammen mit SQL sicher anwenden, um Daten aus einer Datenbank zu lesen, neue Daten zu schreiben, bestehende Daten zu aktualisieren oder zu löschen.
- Du kannst Geschäfts- und Persistenzlogik in gemeinsamen Modell-Klassen implementieren.
- Du kannst erklären, wie objektrelationales Mapping funktioniert.
- Du kannst objektrelationales Mapping einsetzen, um Datenbankzugriffe zu abstrahieren.

# Du wirst folgende Fertigkeiten lernen

Du kannst ...

- Daten aus Formularen verarbeiten und in einer Datenbank speichern.
- PHP zusammen mit SQL-SELECT sicher einsetzen um Daten aus einer Datenbank zu lesen und zu verarbeiten.
- PHP zusammen mit SQL-INSERT oder UPDATE sicher einsetzen um Daten in eine Datenbank zu schreiben.
- PHP zusammen mit SQL-DELETE sicher einsetzen um Daten aus einer Datenbank zu löschen.
- Interfaces und Vererbung in PHP einsetzen.
- Prepared Statements verwenden um einen sicheren Datenbankzugriff zu realisieren.
- Objektrelationales Mapping einsetzen, um Datensätze in Objekte umzuwandeln - und umgekehrt.

# Du wirst folgende Fakten lernen

Du kennst ...

- die wichtigsten Datenbankoperationen (SQL-CRUD) von relationalen Datenbanken.
- die Vorgangsweise zur Verarbeitung von relationalen Abfrageergebnissen in PHP.
- den Unterschied von prepare, execute und fetch.
- Grundprinzipien, Vorteile und Nachteile von objektrelationalem Mapping.
- den Unterschied zwischen Klassen und Interfaces in PHP.

# Expertenstatements

Programmierexperten sagen folgendes über Datenbankzugriff in PHP:

- Objektrelationales Mapping ermöglicht die Verwendung von Objekten anstelle von SQL-Befehlen.
- SQL-Befehle werden durch objektrelationales Mapping überflüssig.
- CRUD-Operationen sind die Basisoperationen für alle datenbankgestützten Applikationen.
- Eine Webapplikation ohne Datenbankanbindung kann keine sinnvolle Webapplikation sein.
- Prepared Statements verhindern SQL-Injection und müssen daher für jegliche vom Benutzer übermittelte Daten verwendet werden.
