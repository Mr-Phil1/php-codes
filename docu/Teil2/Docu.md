---
title: Objektorientierung
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS; PHP; Objektorientierung
titlepage: true
titlepage-color: "E19494"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

\pagebreak

# Literatur-/ Quellen-/ Abbildungsverzeichnis

## Literaturverzeichnis

- Wikipedia
  - [Objektorientierte Analyse und Design](https://de.wikipedia.org/wiki/Objektorientierte_Analyse_und_Design)
    - [Objektorientierte Analyse](https://de.wikipedia.org/wiki/Objektorientierte_Analyse_und_Design#Objektorientierte_Analyse)
    - [Klassendiagram](https://de.wikipedia.org/wiki/Klassendiagramm)
    - [Sequenzdiagramm](https://de.wikipedia.org/wiki/Sequenzdiagramm)
    - [Zustandsdiagramm (UML)](<https://de.wikipedia.org/wiki/Zustandsdiagramm_(UML)>)
  - [DIN 69901-Projektmanagement](https://de.wikipedia.org/wiki/DIN_69901)
- [UML-Diagrams](https://www.uml-diagrams.org/)
  - [UML Class and Object Diagrams Overview](https://www.uml-diagrams.org/class-diagrams-overview.html)
  - [UML Sequence Diagrams](https://www.uml-diagrams.org/sequence-diagrams.html)
  - [State Machine Diagrams](https://www.uml-diagrams.org/state-machine-diagrams.html)
- [Mr. Phil-GitLab](https://gitlab.com/Mr-Phil1/)
  - [Semesterprojekt-CiscoConfigBuilder](https://gitlab.com/Mr-Phil1/semesterprojekt2ciscoconfig)

## Quellenverzeichnis

- Wikipedia
  - [Objektorientierte Analyse und Design](https://de.wikipedia.org/wiki/Objektorientierte_Analyse_und_Design)
    - [Objektorientierte Analyse](https://de.wikipedia.org/wiki/Objektorientierte_Analyse_und_Design#Objektorientierte_Analyse)
- [UML-Homepage](https://www.uml.org/)
  - [UML-Logo](https://www.omg.org/uml-directory/images/logos/UML-logo.jpg)

## Abbildungsvezeichnis

\renewcommand{\listfigurename}{}
\listoffigures
