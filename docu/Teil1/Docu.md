---
title: PHP Grundlagen
subtitle: Formulare
author: Mr. Phil
rights: Nah
language: de-AT
keywords: POS; PHP; Formulare
titlepage: true
titlepage-color: "E19494"
titlepage-text-color: "000000"
table-use-row-colors: true
toc-own-page: true
---

# HTTP

## HTTP-Statuscode

Die erste Ziffer eines Statuscodes stellt die Statusklasse dar.

### 1xx: Information

Die Bearbeitung der Anfrage dauert noch an.

| Code: | Nachricht: | Beschreibung:                                        |
| ----- | ---------- | ---------------------------------------------------- |
| 100   | Continue   | Die laufende Anfrage wurde noch nicht zurückgewissen |

### 2xx: Erfolgreiche Operation

Die Anfrage war erfolgreich, die Antwort kann verwertet werden.

| Code: | Nachricht: | Beschreibung:               |
| ----- | ---------- | --------------------------- |
| 200   | OK         | Die Anfrage war erfolgreich |

### 4xx: Client-Fehler

Die Ursache des Scheiterns der Anfrage liegt (eher) im Verantwortungsbereich des Clients.

| Code: | Nachricht:   | Beschreibung:                                                                         |
| ----- | ------------ | ------------------------------------------------------------------------------------- |
| 400   | Bad Request  | Ein fehlerhafter Webseiten Aufruf                                                     |
| 401   | Unauthorized |                                                                                       |
| 403   | Forbidden    | Die gewünschte Webseite kann mangels fehlender Berechtigungen nicht aufgerufen werden |
| 404   | Not Found    | Die angeforderte Webseite wurde nicht gefunden                                        |

### 5xx: Server-Fehler

Nicht klar von den so genannten Client-Fehlern abzugrenzen. Die Ursache des Scheiterns der Anfrage liegt jedoch eher im Verantwortungsbereich des Servers.

| Code: | Nachricht:            | Beschreibung:                                                                                      |
| ----- | --------------------- | -------------------------------------------------------------------------------------------------- |
| 500   | Internal Server Error | Sammel-Statuscode für Servercode                                                                   |
| 502   | Bad Gateway           | Der Server konnte seine Funktion als Gateway nicht erfüllen                                        |
| 503   | Service Unavailable   | Der Server steht temporär nicht zur Verfügung                                                      |
| 504   | Gateway Timeout       | Der Server konnte seine Funktion innerhalb einer definierten Zeitspanne als Gateway nicht erfüllen |

\pagebreak

## HTTP Request Methods

Die meist verwendeten Methoden sind **GET** und **POST**

### GET

```html
/test/demo_form.php?name1=value1&name2=value2
```

Diese Anfragen:

- können gecached werden
- können im Browserverlauf bleiben
- können als Lesezeichen gespeichert werden
- sollten nicht für sensible Daten verwendet werden
- haben eine Längenbeschränkung

### POST

```html
POST /test/demo_form.php HTTP/1.1 Host: w3schools.com name1=value1&name2=value2
```

Diese Anfragen:

- können nicht gecached werden
- bleiben nicht im Browserverlauf
- können als Lesezeichen gespeichert werden
- sollten für sensible Daten verwendet werden
- haben keine Längenbeschränkung

### PUT

### HEAD

### DELETE

### PATCH

### OPTIONS

![Compare GET vs. POST [Quelle: W3Schools HTTP Request Methods]](Compare-GET-vs.-POST.png)

\pagebreak

# HTML

Mit Hilfe von HTML-Dateien können Seiten in Webbrowsern dargestellt werden. Die aktuellste Verstion ist HTML 5.2.

## Syntax für eine Einfache HTML Seite

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Titel der Webseite</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <!-- weitere Kopfinformationen -->
    <!-- Kommentare werden im Browser nicht angezeigt. -->
  </head>
  <body>
    <p>Inhalt der Webseite</p>
  </body>
</html>
```

\pagebreak

# Bootstrap

Bootstrap ist das populärste CSS Framework welches am 19.08.2011 von Twitter veröffentlicht wurde. Die aktuellste Version ist Version 5. Dieses Framework unterstützt denn Programmierer bei der Mobile-First Entwicklung.

\pagebreak

# PHP

PHP ist eine an C und Perl angelehente Skriptsprache. PHP wurde imJahr 1995 von Rasmus Lerdorf veröffentlicht, die aktuellste Version ist Version 8.1.2. Im Vergleich zu HTML wird PHP nicht von einem Client sonder von einem PHP-fähigen Webserver interpretiert.

![Darstellung der Funktionsweise von PHP (Urheber: "MovGP0", Lizenz: CC BY-SA 3.0 [Wikimedia-PHP funktionsweise.svg](hhttps://commons.wikimedia.org/wiki/File:PHP_funktionsweise.svg))](PHP_funktionsweise.png)

## Syntax

Ausgaben von Hello Welt.

```php
<?php
    echo 'Hallo Welt!';
?>
```

\pagebreak

# Literatur-/ Quellen-/ Abbildungsverzeichnis

## Literaturverzeichnis

- [W3Schools](https://www.w3schools.com/)
  - [HTTP Status Messages](https://www.w3schools.com/tags/ref_httpmessages.asp)
  - [HTTP Request Methods](https://www.w3schools.com/tags/ref_httpmethods.asp)
  - [HTML Forms](https://www.w3schools.com/html/html_forms.asp)
  - [Bootstrap 3 Tutorial](https://www.w3schools.com/bootstrap/)
  - [PHP Tutorial](https://www.w3schools.com/php/default.asp)
  - [PHP Form Handling](https://www.w3schools.com/php/php_forms.asp)
- Bootstrap
  - [Get Bootstrap](https://getbootstrap.com/)
  - [Download](https://getbootstrap.com/docs/5.1/getting-started/download/)
  - [Bootstrap Docu](https://getbootstrap.com/docs/5.1/getting-started/introduction/)
- PHP
  - [PHP](https://www.php.net/)
  - [PHP-Handbuch](http://php.net/manual/de/)
- [Wikipedia](https://de.wikipedia.org/)
  - [HTTP-Statuscode](https://de.wikipedia.org/wiki/HTTP-Statuscode)
  - [Hypertext Markup Language](https://de.wikipedia.org/wiki/Hypertext_Markup_Language)

## Abbildungsvezeichnis

\renewcommand{\listfigurename}{}
\listoffigures
