<!DOCTYPE html>
<html lang="de">
<?php
include "../helper/head.php";
?>

<body>

<?php
include "../helper/navbar.php";
?>

<div class="container">
    <div class="row">
        <h2>Beiträge</h2>
    </div>
    <div class="row">
        <p>
            <a href="create.php" class="btn btn-success">Erstellen <span class="glyphicon glyphicon-plus"></span></a>
        </p>

        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Titel</th>
                <th>Inhalt</th>
                <th>Besitzer</th>
                <th>Freigabedatum</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Beitrag 1</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisici elit...</td>
                <td>User 1</td>
                <td>2017-02-05</td>
                <td><a class="btn btn-info" href="view.php?id=29"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                            class="btn btn-primary" href="update.php?id=29"><span
                                class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                            class="btn btn-danger" href="delete.php?id=29"><span
                                class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <tr>
                <td>Beitrag 2</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisici elit...</td>
                <td>User 1</td>
                <td>2017-02-05</td>
                <td><a class="btn btn-info" href="view.php?id=29"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                            class="btn btn-primary" href="update.php?id=29"><span
                                class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                            class="btn btn-danger" href="delete.php?id=29"><span
                                class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <tr>
                <td>Beitrag 3</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisici elit...</td>
                <td>User 2</td>
                <td>2017-02-05</td>
                <td><a class="btn btn-info" href="view.php?id=29"><span
                                class="glyphicon glyphicon-eye-open"></span></a></a>
                </td>
            </tr>
            <tr>
                <td>Beitrag 4</td>
                <td>Lorem ipsum dolor sit amet, consectetur adipisici elit...</td>
                <td>User 1</td>
                <td>2017-02-05</td>
                <td><a class="btn btn-info" href="view.php?id=29"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;<a
                            class="btn btn-primary" href="update.php?id=29"><span
                                class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a
                            class="btn btn-danger" href="delete.php?id=29"><span
                                class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div> <!-- /container -->
</body>
</html>