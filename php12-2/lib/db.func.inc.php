<?php

$dbName = 'php12';
$dbHost = 'mysql:3306';
$dbUsername = 'root';
$dbUserPassword = '123';

/**
 * Verbindung zur DB aufbauen
 * @return PDO (Verbindungsobjekt)
 */
function connect()
{
    global $dbName, $dbHost, $dbUsername, $dbUserPassword, $dsn, $dbPort, $pod;
    try {
        $conn = new PDO("mysql:host=$dbHost;dbname=$dbName", $dbUsername, $dbUserPassword);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // echo "<p class='alert alert-success'>Datenbank Connection erfolgreich |</p>";
        return $conn;
    } catch (PDOException $e) {
        echo "<p class='alert alert-danger'>Datenbank Connection Problem </p>";
        die($e->getMessage());
    }
}

/**
 * Datensatz speichern
 * @param $name
 * @param $date
 * @param $bmi
 */
function save($name, $date, $bmi)
{
    $db = connect();
    $sql = "INSERT INTO bmi (name, date, bmi) values(?, ?, ?)";
    $stmt = $db->prepare($sql);
    $stmt->execute(array($name, $date, $bmi));
}

/**
 * Auslesen aller Daten (als assoziatives Array)
 * @return array
 */
function getAll()
{
    $db = connect();
    $sql = 'SELECT * FROM bmi ORDER BY date DESC ';
    $stmt = $db->prepare($sql);
    $stmt->execute();
    $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $data;
}

?>