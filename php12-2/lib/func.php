<?php
$errors = [];
function validate($name, $measurementDate, $height, $weight)
{

    return validateName($name) & validateMeasurementDate($measurementDate) & validateHeight($height) & validateWeight($weight);
}

function validateName($name)
{
    global $errors;
    if (strlen($name) == 0) {
        $errors['name'] = 'Name darf nicht leer sein';
        return false;
    } else if (strlen($name) > 25) {
        $errors['name'] = 'Name zu lang';
        return false;
    } else {
        return true;
    }
}

function validateMeasurementDate($measurementDate)
{
    global $errors;
    try {
        if ($measurementDate == "") {
            $errors['measurementDate'] = 'Messdatum darf nicht leer sein';
            return false;
        } elseif (new DateTime($measurementDate) > new DateTime()) {
            $errors['measurementDate'] = 'Messdatum darf nicht in der Zukunft liegen';
            return false;
        } else {
            return true;
        }
    } catch (Exception $e) {
        $errors['measurementDate'] = 'Messdatum ungültig';
    }
}

function validateHeight($height)
{
    global $errors;
    if (!is_numeric($height) || $height < 1 || $height > 300) {
        $errors['size'] = 'Die Eingabe der Größe ist ungültig';
        return false;
    } else {
        return true;
    }
}

function validateWeight($weight)
{
    global $errors;
    if (!is_numeric($weight) || $weight < 1 || $weight > 300) {
        $errors['weight'] = 'Die Eingabe des Gewichts ist ungültig';
        return false;
    } else {
        return true;
    }
}

function calculateBmi($height, $weight)
{
    $bmi = $weight / pow(($height / 100), 2);
    return round($bmi, 2);
}

function printBmi($bmi)
{
    $bmi_text = "Ihr BMI beträgt: ";
    if ($bmi < 18.5) {
        echo "<p class='alert alert-warning'>";
        echo $bmi_text;
        echo $bmi." - Untergewicht";
        echo "</p>";
    } elseif ($bmi < 24.9) {
        echo "<p class='alert alert-success'>";
        echo $bmi_text;
        echo $bmi." - Normalgewicht";
        echo "</p>";
    } elseif ($bmi < 29.9) {
        echo "<p class='alert alert-warning'>";
        echo $bmi_text;
        echo $bmi." - Übergewicht";
        echo "</p>";
    } else {
        echo "<p class='alert alert-danger'>";
        echo $bmi_text;
        echo $bmi." - Adipositas";
        echo "</p>";
    }
}