<?php

class CookieHelper
{


    public function __construct()
    {
    }


    /**
     * @return bool
     */
    public static function isAllowed()
    {
        return isset($_SESSION['allowed']);
    }

    /**
     * setzt im $_SESSION Array beim index ['allowed'] die Parameter mitgabe (true/false)
     * @param bool $allowed
     */
    public function setAllowed(bool $allowed)
    {
        $_SESSION['allowed'] = $allowed;
    }

}