<?php
session_start();
require_once "models/User.php";
require_once "models/CookieHelper.php";
require_once "lib/debug.php";
CookieHelper::isAllowed(false);
if (!User::isLoggedIn()) {
    console_log("User: it works.", true);
    console_log("user is logged out", true);
}
if (isset($_POST['logout'])) {
   User::logout();
    header("Location: index.php");
} else {
    http_response_code(405);
}