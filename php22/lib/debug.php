<?php
// https://stackify.com/how-to-log-to-console-in-php/ [Abgerufen am 2021-11-20]
function console_log($output, $show, $with_script_tags = true)
{
    if ($show) {
        $js_code = 'console.log(' . json_encode($output, JSON_HEX_TAG) .
            ');';
        if ($with_script_tags && $show) {
            $js_code = '<script>' . $js_code . '</script>';
        }
        echo $js_code;
    }
}