<?php
session_start();
require_once "models/User.php";
require_once "models/CookieHelper.php";
require_once "lib/debug.php";


if (!(CookieHelper::isAllowed() && User::isLoggedIn())) {
    header("Location: index.php");
    console_log("User not logged in", true);
}

?>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Wochenkarte</title>
</head>
<body>
<div class="container">
    <div class="row d-flex justify-content-center">

        <div class="col-sm m-3 border border-2 rounded align-content-center bg-light bg-gradient bg-opacity-75 shadow-lg rounded-2 ">
            <form id='logout_form' method="post" action="logout.php">
                <div class="row justify-content-md-center mt-2">
                    <div class="form-group">
                        <input type="submit" name="logout" class="btn btn-secondary btn-block" value="Logout">
                    </div>
                </div>
            </form>

            <h1 class="text-center align-content-center mt-3">Wochenkarte</h1>
            <div class="row text-center mt-5">
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <h4>Montag</h4>
                    <img src="img/01-montag.jpg" alt="" width="250px">
                    <p>Photo by <a
                                href="https://unsplash.com/@lvnatikk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Lily
                            Banse</a> on <a
                                href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
                    </p>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <h4>Dienstag</h4>
                    <img src="img/02-dienstag.jpg" alt="" width="250px">
                    <p>Photo by <a
                                href="https://unsplash.com/@briewilly?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Chad
                            Montano</a> on <a
                                href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
                    </p>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <h4>Mittwoch</h4>
                    <img src="img/03-mittwoch.jpg" alt="" width="250px">
                    <p>Photo by <a
                                href="https://unsplash.com/@simplethemes?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Casey
                            Lee</a> on <a
                                href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
                    </p>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <h4>Donnerstag</h4>
                    <img src="img/04-donnerstag.jpg" alt="" width="250px">
                    <p>Photo by <a
                                href="https://unsplash.com/@cant89?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Davide
                            Cantelli</a> on <a
                                href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
                    </p>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <h4>Freitag</h4>
                    <img src="img/05-freitag.jpg" alt="" width="250px">
                    <p>Photo by <a
                                href="https://unsplash.com/@1ncreased?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Lidye</a>
                        on
                        <a href="https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
                    </p>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-12">
                    <h4>Samstag</h4>
                    <p>Ruhetag</p>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
