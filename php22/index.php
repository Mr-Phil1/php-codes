<?php
session_start();
require_once "models/User.php";
require_once "models/CookieHelper.php";
require_once "lib/debug.php";
$user = new User("", "");
$cookie = new CookieHelper();
$message = '';

//$_SESSION['allowed'] = true;
if (isset($_POST['allow'])) {
    $cookie->setAllowed(true);
}

if (isset($_POST['login'])) {
    $ceck = new User(htmlspecialchars($_POST['email']), htmlspecialchars($_POST['password']));
    if ($ceck->validate()) {
        $user = $ceck;
        setcookie('email', $user->getEmail(), time() + (300), "/");
        setcookie('password', $user->getPassword(), time() + (300), "/");
        $user->login();
    } else {
        $message = "<p class='text-danger'>Zugangsdaten ungültig!</p>";
    }
}

if (CookieHelper::isAllowed()) {
    console_log("CookieHelper: it works.", true);
    console_log("cookie is allow", true);
}

if (User::isLoggedIn()) {
    console_log("User: it works.", true);
    console_log("user is logged in", true);
} else {

    console_log("user is not logged in", true);
}

?>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Wochenkarte</title>
</head>
<body>
<div class="container">
    <div class="row d-flex justify-content-center">
        <div class="col-sm m-3 border border-2 rounded align-content-center bg-light bg-gradient bg-opacity-75 shadow-lg rounded-2 ">
            <h1 class="text-center align-content-center">Wochenkarte</h1>
            <?php
            if (!CookieHelper::isAllowed()){
            ?>

            <h2 class="mt-5 text-center align-content-center">Willkommen</h2>
            <h5 class="mt-5 text-center align-content-center">Diese Website verwendet Cookies.</h5>

            <form id="form_grade" action="index.php" method="post">
                <div class="row d-flex justify-content-center">
                    <div class="row d-flex justify-content-center">
                        <input type="submit" name="allow" class="btn btn-warning btn-block" value="Akzeptieren">
                    </div>
                </div>
            </form>
        </div>
        <?php
        } elseif (CookieHelper::isAllowed() && !User::isLoggedIn()) {

        ?>
        <h3 class="mx-2 mt-5 text-right">Bitte Anmelden</h3>

        <form id="login_form" action="index.php" method="post">
            <div class="row d-flex justify-content-center">
                <div class="row">
                    <div class=" form-group">
                        <label for="email">E-Mail Addresse*</label>
                        <input type="email" name="email" id="email"
                               class="form-control <?= $user->hasError('email') ? "is-invalid" : "" ?>"
                               maxlength="28" required
                               value="<?php if (isset($_COOKIE['email'])) {
                                   echo $_COOKIE['email'];
                               }?>">
                    </div>
                </div>
                <div class='row justify-content-center'>
                    <?= $message ?>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div>
                            <label for="password">Password*</label>
                            <input type="password" name="password" id="password"
                                   class="form-control  <?= $user->hasError('password') ? "is-invalid" : "" ?> "
                                   required
                                   value="<?php if (isset($_COOKIE['password'])) {
                                       echo $_COOKIE['password'];
                                   }?>">
                        </div>
                    </div>
                </div>
                <div class="">
                    <div class="row mt-3 d-flex justify-content-center">
                        <div class="row d-flex justify-content-center">
                            <input type="submit" name="login" class="btn btn-primary btn-block" value="Anmeldung">
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
</form>
</div>
<?php
} else {
  header("Location: wochenkarte.php");
}
?>
</div>
</div>
</div>
</body>
</html>